#include <HoneywellHPM.h>

#define WATER_SENSE_PIN     2

HoneywellHPM sensor = HoneywellHPM(Serial1);

void processParticleMeasure(void) {
  hw_particle_reading_t reading;
  
  if (sensor.pollParticleResult()) {
    sensor.getLastReading(reading);
    Serial.println("Reading Available:");

    Serial.print(" - Particle 2.5: ");
    Serial.print(reading.particle_2_5);
    Serial.println("");

    Serial.print(" - Particle 10: ");
    Serial.print(reading.particle_10);
    Serial.println("");
  }
}


void processWaterSensor() {
  static bool state = false;

  bool newstate = digitalRead(WATER_SENSE_PIN) == 0;
  if (state != newstate) {
    state = newstate;
    Serial.print("Water sensor: ");
    if (state) {
      Serial.print("FLOODING");
    }
    else {
      Serial.print("DRY");
    }
    Serial.println(" ");
  }
  
}

void setup() {
  // Start serial com, and init sensor
  Serial.begin(9600);
  sensor.begin();

  // Set up a GPIO for Watersensor
  pinMode(WATER_SENSE_PIN, INPUT_PULLUP);

  // Wait to get serial output correct
  delay(2000);
  Serial.println("Starting application");

  Serial.println("Starting Measurement"); 
  sensor.startMeasurement();
}

void loop() {
  
  processParticleMeasure();

  processWaterSensor();
  
}


