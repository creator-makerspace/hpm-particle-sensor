/*
 * HoneywellHPM.cpp
 *
 *  Created on: Nov 24, 2017
 *      Author: johnny
 */

#include "HoneywellHPM.h"

#define HPM_BAUDRATE	9600
#define TIMEOUT			100


HoneywellHPM::HoneywellHPM(HardwareSerial &serial)
: _serial(serial), _txchecksum(0)
{
	// Set the header in the buffer
}


void HoneywellHPM::begin() {
	_serial.begin(HPM_BAUDRATE);

	_last_reading.particle_10 = 0;
	_last_reading.particle_2_5 = 0;
}

bool HoneywellHPM::startMeasurement() {
	_sendHeader();
	_sendByte(0x01);
	_sendByte(0x01);
	_sendChecksum();

	return _readAcknowledge();
}


bool HoneywellHPM::enableAutoSend(void) {
	_sendHeader();
	_sendByte(0x01);
	_sendByte(0x40);
	_sendChecksum();

	return _readAcknowledge();
}


bool HoneywellHPM::pollParticleResult(void) {
	// Simply check for a packet ACK message
	return _readSensorResponse(false) == HW_COMSTATUS_PACKET_ACK;
}

void HoneywellHPM::getLastReading(hw_particle_reading_t & particleresult) {
	particleresult.particle_2_5 = _last_reading.particle_2_5;
	particleresult.particle_10 = _last_reading.particle_10;
}


/*
bool HoneywellHPM::stopMeasurement() {
	_sendHeader();
	_sendByte(0x01);
	_sendByte(0x02);
	_sendChecksum();

	return _readAcknowledge();
}

bool HoneywellHPM::stopAutoSend(void) {
	_sendHeader();
	_sendByte(0x01);
	_sendByte(0x20);
	_sendChecksum();

	return _readAcknowledge();
}


bool HoneywellHPM::readParticleResults(hw_particle_reading_t &particleresult) {
	_sendHeader();
	_sendByte(0x01);
	_sendByte(0x04);
	_sendChecksum();

	// Read a packet
	uint8_t rlen;
	hw_comstatus_t cstat = _readPacket(&rlen);
	Serial.println("");
	if (cstat != HW_COMSTATUS_OK) {
		return false;
	}
	if (rlen != 5) {
		return false;
	}

	// We likely got a reading
	particleresult.particle_2_5 = (_rxbuffer[2] << 8) | _rxbuffer[3];
	particleresult.particle_10  = (_rxbuffer[4] << 8) | _rxbuffer[5];

	return true;
}

bool HoneywellHPM::setAdjustmentCoeff(uint8_t coeff) {
	// TODO: Implement
	return false;
}

bool HoneywellHPM::getAdjustmentCoeff(uint8_t &target_coeff) {
	// TODO: Implement
	return false;
}
*/

void HoneywellHPM::_sendHeader(void) {
	// Write the header
	static const uint8_t HEADER = 0x68;

	_serial.write(HEADER);
	_txchecksum += HEADER;
}

void HoneywellHPM::_sendByte(uint8_t data) {
	_serial.write(data);
	_txchecksum += data;
}

void HoneywellHPM::_sendChecksum() {
	uint8_t chkbyte = (-_txchecksum) & 0xFF;
	_serial.write(chkbyte);
}

bool HoneywellHPM::_readByteTimeout(uint8_t &readbyte) {
	int data = -1;
	unsigned long starttime = millis();
	while (true) {
		data = _serial.read();
		if (data != -1) break;
		if ((millis() - starttime) > TIMEOUT) return false;
	}

//	Serial.print(data, HEX);
//	Serial.print(" ");

	readbyte = (uint8_t)data;
	return true;
}

hw_comstatus_t HoneywellHPM::_readComplete_AckNackPacket(uint8_t firstbyte) {
	// Read byte
	uint8_t rdata;

	if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;

	// If data byte is not the same, we have a sync issue
	if (rdata != firstbyte) return HW_COMSTATUS_SYNC;

	if (rdata == 0xA5) return HW_COMSTATUS_PACKET_ACK;
	return HW_COMSTATUS_PACKET_NACK;
}

hw_comstatus_t HoneywellHPM::_readComplete_ResponsePacket(uint8_t firstbyte) {
	uint16_t checksum = firstbyte;
	uint8_t rdata;
	uint8_t rxix = 0;

	// Get the length
	if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
	checksum += rdata;
	uint8_t remainlen = rdata;

	do {
		// Just for sanity
		if (rxix >= sizeof(_rxbuffer)) return HW_COMSTATUS_SYNC;

		if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
		_rxbuffer[rxix++] = rdata;
		checksum += rdata;
	}
	while (--remainlen);

	// Read the check byte
	uint8_t checkrecv = (uint8_t)_serial.read();

	// Calculate the checksum, and verify it
	uint8_t checkcalc = (-checksum) & 0xFF;
	if (checkrecv != checkcalc) return HW_COMSTATUS_CRC_ERROR;

	// We have a working packet, so return ACK
	return HW_COMSTATUS_PACKET_ACK;
}

hw_comstatus_t HoneywellHPM::_readComplete_AutoPacket(uint8_t firstbyte) {
	uint16_t checksum = firstbyte;
	uint8_t rdata;
	uint8_t rxix = 0;
	uint8_t bix = 3;

	// Get header byte 2
	if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
	if (rdata != 0x4D) return HW_COMSTATUS_SYNC;
	checksum += rdata;

	// Discard Length High byte, and read the low byte
	if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
	checksum += rdata;
	if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
	checksum += rdata;

	// Keep the remaning length
	hw_particle_reading_t autoread;
	uint8_t remainlen = rdata;
	uint16_t checkrecv;

	do {
		// Read data byte
		if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;
		bix++;

		// Do not include the last two bytes in checksum
		if (bix < 30) checksum += rdata;
/*
		if (bix >= 6 && bix <= 9) {
			Serial.print(rdata);
			Serial.println("");
		}
*/
		// Process the bytes
		switch (bix) {
		case 6: autoread.particle_2_5 = ((uint16_t)rdata) << 8; break;
		case 7: autoread.particle_2_5 |= rdata; break;
		case 8: autoread.particle_10  = ((uint16_t)rdata) << 8; break;
		case 9: autoread.particle_10  |= rdata; break;

		case 30: checkrecv =  ((uint16_t)rdata) << 8; break;
		case 31: checkrecv |= rdata; break;

		default: break;
		}
	}
	while (--remainlen);

	if (checksum != checkrecv) return HW_COMSTATUS_CRC_ERROR;

	// Update the last reading
	_last_reading.particle_2_5 = autoread.particle_2_5;
	_last_reading.particle_10 = autoread.particle_10;


	return HW_COMSTATUS_PACKET_ACK;
}


hw_comstatus_t HoneywellHPM::_readSensorResponse(bool request) {

	uint8_t rdata;
	hw_comstatus_t status = HW_COMSTATUS_SYNC;

	// Read until we reach a viable byte
	while (status == HW_COMSTATUS_SYNC) {
		// Read byte
		if (!_readByteTimeout(rdata)) return HW_COMSTATUS_TIMEOUT;

		// Check valid bytes
		switch (rdata) {
		case 0xA5:	// ACK Packet start
		case 0x96:  // Nack packet start
			status = _readComplete_AckNackPacket(rdata);
			if (status == HW_COMSTATUS_PACKET_ACK || status == HW_COMSTATUS_PACKET_NACK) {
				// We are done
				return status;
			}
			break;

		case 0x40:
			// Ignore if we are not in a request
			if (!request) break;

			// If request, process the packet
			status = _readComplete_ResponsePacket(rdata);
			if (status == HW_COMSTATUS_PACKET_ACK ||
				status == HW_COMSTATUS_PACKET_NACK ||
				status == HW_COMSTATUS_CRC_ERROR)
			{
				return status;
			}
			break;

		case 0x42:
			// Process auto reading
			status = _readComplete_AutoPacket(rdata);
			if (status == HW_COMSTATUS_PACKET_ACK ||
				status == HW_COMSTATUS_CRC_ERROR)
			{
				if (!request) return status;
			}
		}
	}

	return status;
}

bool HoneywellHPM::_readAcknowledge(void) {
	uint8_t len;
	// Read packet, and return false on timeout
	hw_comstatus_t status = _readSensorResponse(true);
	return status == HW_COMSTATUS_PACKET_ACK;
}




