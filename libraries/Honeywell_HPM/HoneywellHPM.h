/*
 * HoneywellHPM.h
 *
 *  Created on: Nov 24, 2017
 *      Author: johnny
 */

#ifndef HONEYWELLHPM_H_
#define HONEYWELLHPM_H_

#include <Arduino.h>
#include <stdint.h>

typedef struct {
	uint16_t particle_2_5;
	uint16_t particle_10;
}
hw_particle_reading_t;


typedef enum {
	HW_COMSTATUS_SYNC,
	HW_COMSTATUS_PACKET_ACK,
	HW_COMSTATUS_PACKET_NACK,
	HW_COMSTATUS_CRC_ERROR,
	HW_COMSTATUS_TIMEOUT
}
hw_comstatus_t;

class HoneywellHPM {
public:
	HoneywellHPM(HardwareSerial &serial);

	void 		begin(void);

	bool 		startMeasurement(void);

	bool 		enableAutoSend(void);

	bool		pollParticleResult(void);

	void		getLastReading(hw_particle_reading_t & particleresult);

	/*
	bool 		stopMeasurement(void);


	bool 		stopAutoSend(void);

	bool 		readParticleResults(hw_particle_reading_t & particleresult);

	bool 		setAdjustmentCoeff(uint8_t coeff);
	bool		getAdjustmentCoeff(uint8_t & target_coeff);
	*/
private:
	HardwareSerial	    &_serial;
	uint16_t			_txchecksum;

	// RX Buffer
	uint8_t				  _rxbuffer[5];
	hw_particle_reading_t _last_reading;

	// Send methods
	void		_sendHeader(void);
	void 		_sendByte(uint8_t data);
	void 		_sendChecksum(void);

	bool		_readByteTimeout(uint8_t &byte);

	// Receive methods
	hw_comstatus_t _readComplete_AckNackPacket(uint8_t firstbyte);
	hw_comstatus_t _readComplete_ResponsePacket(uint8_t firstbyte);
	hw_comstatus_t _readComplete_AutoPacket(uint8_t firstbyte);

	hw_comstatus_t				_readSensorResponse(bool request);
	bool						_readAcknowledge(void);
};



#endif /* HONEYWELLHPM_H_ */
